import Faq from "../components/Faq";
import Slideshow from "../components/Slideshow";
import Stars from "../components/Stars";
import Tag from "../components/Tag";
import User from "../components/User";
import '../styles/place.scss';
import { useLoaderData } from "react-router-dom";

export default function PlacePage() {

    const logement = useLoaderData()

    return (
        
        <div className="place container">
            <Slideshow images={logement.pictures} />

            <div className="infos">

                <div className="infos-place">
                    <h1>{logement.title}</h1>
                    <h2>{logement.location}</h2>
                    
                    <div className="tags">
                        {logement.tags.map((tag, index) => <Tag key={`tag-${index}`} text={tag} />)}
                    </div>
                </div>
                
                <div className="infos-host">
                    <Stars rating={logement.rating} />
                    <User name={logement.host.name} picture={logement.host.picture} />
                </div>
            </div>

            <div className="details">
                <Faq title="Description" open={true}>
                    {logement.description}
                </Faq>

                <Faq title="Équipements" open={true}>
                    <ul>
                        {logement.equipments.map((equipment, index) => <li key={`equipment-${index}`}>{equipment}</li>)}
                    </ul>
                </Faq>
            </div>


        </div>
    )
}