import Header from '../components/Header';
import '../styles/error.scss'
import { Link } from "react-router-dom";

export default function Error404Page() {
    return (
        <>
            <Header />
            <div className="error-page">
                <h1>404</h1>
                <div>Oups! La page que vous demandez n'existe pas.</div>
                <Link to={`/`}>Retourner sur la page d’accueil</Link>
            </div>
        </>
    )
}