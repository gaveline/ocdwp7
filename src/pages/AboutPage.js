import BigImage from "../components/BigImage";
import Faq from "../components/Faq";
import image from '../images/kalen-emsley-Bkci_8qcdvQ-unsplash 2.png'
import '../styles/about.scss'

export default function AboutPage() {
    return (
        <div className="container">
            <BigImage image={image} overlay={true} />
            <div className="accordeon">
                <Faq title="Fiabilité" open={true}>
                    La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire ou de perturbation du voisinage entraînera une exclusion de notre plateforme.
                </Faq>

                <Faq title="Respect">
                    La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire ou de perturbation du voisinage entraînera une exclusion de notre plateforme.
                </Faq>

                <Faq title="Service">
                    La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire ou de perturbation du voisinage entraînera une exclusion de notre plateforme.
                </Faq>

                <Faq title="Sécurité">
                    La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire ou de perturbation du voisinage entraînera une exclusion de notre plateforme.
                </Faq>
            </div>
        </div>
    )
}