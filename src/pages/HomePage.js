import BigImage from "../components/BigImage";
import image from '../images/eric-muhr-P_XxsdVgtpQ-unsplash.png'
import Thumbnail from "../components/Thumbnail";
import '../styles/homepage.scss';
import { useLoaderData } from "react-router-dom";

export default function HomePage() {

    const logements = useLoaderData();

    return (
        <div className="container">
            <BigImage image={image} text="Chez vous, partout et ailleurs" overlay={true} />
            <div className="logements">
                { logements.map(logement => <Thumbnail key={logement.id} place={logement}/>) }
            </div>
        </div>
    )
}