import { createBrowserRouter } from "react-router-dom";
import Layout from "./layouts/Layout";
import HomePage from "./pages/HomePage";
import Error404Page from "./pages/Error404Page";
import AboutPage from "./pages/AboutPage";
import PlacePage from "./pages/PlacePage";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <Error404Page />,
        children: [
            {
                path: "",
                element: <HomePage />,
                loader: async () => {
                    return fetch('/logements.json').then(res => res.json());
                }
            },
            {
                path: "place/:id",
                element: <PlacePage />,
                loader: async ({ params }) => {
                    return fetch('/logements.json')
                        .then(res => res.json())
                        .then(json => json.find(el => el.id === params.id));
                }
            },
            {
                path: "a-propos",
                element: <AboutPage />,
            }
        ]
    },
]);

export default router;