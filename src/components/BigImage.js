import '../styles/big-image.scss';

export default function BigImage({image, text, overlay, size}) {
    const classes = ['big-image'];

    if(overlay) {
        classes.push('with-overlay')    
    }   

    if(size) {
        classes.push(`size-${size}`)
    }   

    return (
        <div className={classes.join(' ')}>
            <img src={image} alt="" />
            { text && <span>{text}</span> }
        </div>
    )
}