import { Link } from 'react-router-dom';
import '../styles/thumbnail.scss'

export default function Thumbnail({place}) {
    return (
        <Link to={`/place/${place.id}`}>
            <div className='thumbnail'>
                <img src={place.cover} alt="" />
                <span>{place.title}</span>
            </div>
        </Link>
    );
}