import Star from '../images/Star';
import '../styles/stars.scss';

export default function Stars({ rating }) {
    return (
        <div className="stars">
            { 
                Array(5)
                    .fill(0)
                    .map((_, index) => <Star key={`star-${index}`} color={index < rating ? '#FF6060' : '#E3E3E3'} />) 
            }
        </div>
    )
}