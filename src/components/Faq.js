import { useState } from 'react'
import '../styles/faq.scss'
import ChevronRound from '../images/ChevronRound';

export default function Faq({ title, children, open }) {
    const [isOpen, setOpen] = useState(open);

    const classes = ['faq']
    classes.push(isOpen ? 'faq-opened' : 'faq-closed')

    return (
        <div className={classes.join(' ')}>
            <h2 onClick={() => setOpen(!isOpen)}>
                {title}
                <ChevronRound height={13} width={24}/>
            </h2>
            { isOpen ? <div className="content">{children}</div> : false }
        </div>
    )
}