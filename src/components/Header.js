import { Link, NavLink } from 'react-router-dom'
import '../styles/header.scss'
import Logo from '../images/Logo'

export default function Header() {
    return (
        <header>
            <div className="container">
                <Link to={`/`}>
                    <Logo color="#FF6060" width="210" />
                </Link>
                <nav>
                    <ul>
                        <li>
                            <NavLink to={`/`}>Accueil</NavLink>
                        </li>
                        <li>
                            <NavLink to={`/a-propos`}>À propos</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
}
