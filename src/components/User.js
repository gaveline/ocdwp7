import '../styles/user.scss'

export default function User({ name, picture }) {
    const [firstName, lastName] = name.split(' ');

    return (
        <div className='user'>
            <div className='user-name'>{firstName}<br />{lastName}</div>
            <img src={picture} alt="" />
        </div>
    )
}