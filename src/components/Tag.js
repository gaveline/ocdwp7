import '../styles/tag.scss'

export default function Tag({ text }) {
    return (
        <div className='tag'>{text}</div>
    )
}