import { useState } from 'react';
import '../styles/slideshow.scss';
import ChevronSquare from '../images/ChevronSquare';

export default function Slideshow({images}) {

    const [current, setCurrent] = useState(0);

    const gotoPrevious = () => {
        if(current === 0) {
            setCurrent(images.length - 1)
        } 
        else {
            setCurrent(current - 1)
        }
    }

    const gotoNext = () => {
        if(current === images.length-1) {
            setCurrent(0)
        } 
        else {
            setCurrent(current + 1)
        }
    }

    return (
        <div className={`slideshow`}>
            { images.map((image, index) => index === current && <img key={`img-${index}`} src={image} alt="" />) }
            <div className='counter'>{current+1}/{images.length}</div>
            <div className='previous' onClick={gotoPrevious}>
                <ChevronSquare height={80} width={45}/>
            </div>
            <div className='next' onClick={gotoNext}>
                <ChevronSquare height={80} width={45}/>
            </div>
        </div>
    )
}