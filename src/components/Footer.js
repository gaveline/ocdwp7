import '../styles/footer.scss'
import Logo from '../images/Logo'
import { Link } from 'react-router-dom'

export default function Footer() {
    return (
        <footer>
            <div className="container">
                <Link to={`/`}>
                    <Logo color="#fff" width="110" />
                </Link>
                <div>© 2020 Kasa. All rights reserved</div>
            </div>
        </footer>
    )
}